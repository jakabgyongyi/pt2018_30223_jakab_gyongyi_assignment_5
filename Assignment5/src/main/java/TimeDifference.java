public class TimeDifference {
    private long year;
    private long month;
    private long day;
    private long hour;
    private long minute;
    private long second;

    public TimeDifference(long year, long month, long day, long hour, long minute, long second) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public long getMonth() {
        return month;
    }

    public void setMonth(long month) {
        this.month = month;
    }

    public long getDay() {
        return day;
    }

    public void setDay(long day) {
        this.day = day;
    }

    public long getHour() {
        return hour;
    }

    public void setHour(long hour) {
        this.hour = hour;
    }

    public long getMinute() {
        return minute;
    }

    public void setMinute(long minute) {
        this.minute = minute;
    }

    public long getSecond() {
        return second;
    }

    public void setSecond(long second) {
        this.second = second;
    }

    public TimeDifference addition(TimeDifference other){
        long localTimeUnit= this.second + other.getSecond();
        while(localTimeUnit >= 60){
            this.minute ++;
            localTimeUnit -= 60;
        }
        this.second = localTimeUnit;
        localTimeUnit = this.minute + other.getMinute();
        while(localTimeUnit >= 60){
            this.hour ++;
            localTimeUnit -= 60;
        }
        this.minute = localTimeUnit;
        localTimeUnit = this.hour + other.getHour();
        while(localTimeUnit >= 24){
            this.day ++;
            localTimeUnit -= 24;
        }
        this.hour = localTimeUnit;
        this.day += other.getDay();
        this.month += other.getMonth();
        this.year += other.getYear();
        return this;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        if(year != 0)sb.append("Years: ").append(year).append("\t");
        if(month != 0)sb.append("Months: ").append(month).append("\t");
        if(day != 0)sb.append("Days: ").append(day).append("\t");
        if(hour != 0)sb.append("Hours: ").append(hour).append("\t");
        if(minute != 0)sb.append("Minutes: ").append(minute).append("\t");
        if(second != 0)sb.append("Seconds: ").append(second).append("\t");
        return sb.toString();
    }
}
