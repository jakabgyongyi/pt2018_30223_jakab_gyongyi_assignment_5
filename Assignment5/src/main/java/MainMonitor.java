import java.io.*;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainMonitor {
    ArrayList<MonitorData> data;

    public MainMonitor(String fileName){
        data = new ArrayList<>();
        try(Stream<String> stream = Files.lines(Paths.get(fileName))){
            stream.forEach(str-> {
                        String[] splitResult = str.split("\t\t");
                        data.add(new MonitorData(splitResult[0], splitResult[1], splitResult[2])); });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getActivities(){
        ArrayList<String> activities = new ArrayList<>();
        data.forEach(value->activities.add(value.getActivity()));
        return activities;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        this.data.forEach(k-> sb.append(k).append("\n"));
        return sb.toString();
    }

    /**
     * Task 1: Count distinct days that appear in the monitoring data*/
    public long countDistinctDays(){
        List<LocalDateTime> startTimes = data.stream().map(MonitorData::getStart).collect(Collectors.toList());
        List<Integer> allDays = startTimes.stream().map(LocalDateTime::getDayOfMonth).collect(Collectors.toList());
        return allDays.stream().distinct().count();
    }

    /**
     * Task 2: Number of accurences for a activity during the monitoring data*/
    public Map<String, Integer> activityCount(){
        Map<String, Integer> activityCount = getActivities().stream().collect(Collectors.groupingBy(
                Function.identity(),
                Collectors.reducing(0, str->1, Integer::sum)));
        return activityCount;
    }

    public String convertActivityCount(){
        Map<String, Integer> activityCount = activityCount();
        StringBuilder sb =  new StringBuilder();
        activityCount.forEach((k,v)-> {
            sb.append("Activity: ").append(k).append("\n");
            sb.append("Occurrence: ").append(v).append("\n");
        });
        return sb.toString();
    }

    /**
     * Task 3: Activity count for each day of the log*/
    public Map<Integer, Map<String, Long>> activityCountByDays(){
        Map<Integer,Map<String,Long>> activityCountByDays = data
                .stream()
                .collect(
                Collectors.groupingBy(
                    MonitorData::getDay,
                    Collectors.groupingBy(
                        MonitorData::getActivity,
                        Collectors.counting())));
        return activityCountByDays;
    }

    public String convertActivityCountByDays(){
        Map<Integer, Map<String, Long>> something = activityCountByDays();
        StringBuilder sb = new StringBuilder();
        something.forEach((k,v)->{
            sb.append("Day: ").append(k).append("\n");
            v.forEach((key, value)-> sb.append("Activity: ").append(key).append("Count: ").append(value).append("\n"));
        });
        return sb.toString();
    }

    /**
     * Task 4: Total duration of activities during monitoring data*/
    public Map<String, TimeDifference> countActivityTotalDuration(){
        Map<String, TimeDifference> cATD = data
                .stream()
                .collect(
                        Collectors.toMap(MonitorData::getActivity, MonitorData::calculateTimeDifference, TimeDifference::addition))
                .entrySet()
                .stream()
                .filter(value->value.getValue().getHour() < 10 && value.getValue().getDay() == 0)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return cATD;
    }

    public String convertCountActivityTotalDuration(){
        Map<String, TimeDifference> td = countActivityTotalDuration();
        StringBuilder sb = new StringBuilder();
        td.forEach((k,v)-> sb.append("Activity: ").append(k).append("\n").append("Duration: ").append(v).append("\n"));
        return sb.toString();
    }

    /**
     * Task 5: Filter acvities with 90% of monitoring samples and total duration less than 5 min*/
    public List<String> filterActivities(){
        Map<String, Integer> activityCount = activityCount();
        Map<String, TimeDifference> cATD = countActivityTotalDuration();
        double totalNumberActivities = 0.0;
        for(Integer itr: activityCount.values()) totalNumberActivities += itr;
        double finalTotalNumberActivities = totalNumberActivities;
        List<String> filterActivities = cATD.keySet()
                .stream()
                .filter(value -> cATD.get(value).getMinute() < 7 && cATD.get(value).getHour() == 0)
               // .filter(value -> activityCount.containsKey(value) && (activityCount.get(value)/ finalTotalNumberActivities) >= 0.9)
                .collect(Collectors.toList());
        return filterActivities;
    }

    public String convertFilterActivities(){
        StringBuilder sb = new StringBuilder();
        List<String> filterActivities = filterActivities();
        filterActivities.forEach(sb::append);
        return sb.toString();
    }

    public void writeFile(String fileName, String object){
        try {
            Files.write(Paths.get(fileName), Collections.singleton(object));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
