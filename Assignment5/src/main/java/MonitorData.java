
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class MonitorData {
    private LocalDateTime start;
    private LocalDateTime end;
    private String activity;

    public MonitorData(String start, String end, String activity){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.start = LocalDateTime.parse(start, formatter);
        this.end = LocalDateTime.parse(end, formatter);
        this.activity = activity;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getDay(){
        return start.getDayOfMonth();
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        sb.append("Start Time: ").append(formatter.format(this.start)).append("\n");
        sb.append("End Time: ").append(formatter.format(this.end)).append("\n");
        sb.append("Activity: ").append(this.activity).append("\n");
        return sb.toString();
    }

    public TimeDifference calculateTimeDifference(){
        LocalDateTime temp = LocalDateTime.from(start);
        long years = temp.until(end, ChronoUnit.YEARS);
        temp = temp.plusYears(years);

        long months = temp.until(end, ChronoUnit.MONTHS);
        temp = temp.plusMonths(months);

        long days = temp.until(end, ChronoUnit.DAYS);
        temp = temp.plusDays(days);

        long hours = temp.until(end, ChronoUnit.HOURS);
        temp = temp.plusHours(hours);

        long minutes = temp.until(end, ChronoUnit.MINUTES);
        temp = temp.plusMinutes(minutes);

        long seconds = temp.until(end, ChronoUnit.SECONDS);

        return new TimeDifference(years, months, days, hours, minutes, seconds);
    }
}
